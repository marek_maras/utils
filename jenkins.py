import os
from collections import OrderedDict


def create_http_post_request(job_url, params_dict, token):
    request_url = "{}/buildWithParameters?token={}".format(job_url, token)
    params_od = OrderedDict(sorted(params_dict.items()))
    params_list = ["{param}={value}".format(param=k, value=v) for k, v in params_od.iteritems()]
    params_curl = "&".join(params_list)
    return r'curl -X POST -v -d "{data}" {url}'.format(data=params_curl, url=request_url)


def trigger_jenkins_build_with_parameters(job_url, params_dict, token):
    command = create_http_post_request(job_url, params_dict, token)
    print(command)
    os.system(command)
