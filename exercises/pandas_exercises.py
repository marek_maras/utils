#!/usr/bin/env python
# -*- coding: utf-8 -*-

# based on https://pynative.com/python-pandas-exercise/
import pandas as pd

CSV_PATH = "./Automobile_data.csv"


def ex1():
    df = pd.read_csv(CSV_PATH)
    print(df.head(5))
    print(df.tail(5))


def ex2():
    df = pd.read_csv(CSV_PATH,na_values={
        'price': ["?", "n.a"],
        'stroke': ["?", "n.a"],
        'horsepower': ["?", "n.a"],
        'peak-rpm': ["?", "n.a"],
        'average-mileage': ["?", "n.a"]})
    print (df)


def ex3():
    df = pd.read_csv(CSV_PATH)
    series = (df.price == df['price'].max())

    print(type(series))
    print(series.head(5))
    print("...")
    print(series.tail(5))
    print("")

    print("df[['company', 'price']] is {}".format(type(df[['company', 'price']])))
    print(type(df[['company', 'price']][series]))
    print(df[['company', 'price']][series])
    print("")
    print(type(df[series]))
    print(df[series])


def ex4(df):
    print("----------")
    print("my solution:")
    toyota_series = df["company"] == "toyota"
    print(df[toyota_series])
    print("----------")
    print("from page:")
    company_groupby = df.groupby('company')
    print(type(company_groupby))
    print(company_groupby.get_group('toyota'))


def ex5(df):
    print("----------\n"
          "my solution:\n"
          "----------")
    companies = df["company"].unique().tolist()
    for company in companies:
        print("{}: {}".format(company, (df["company"] == company).tolist().count(True)))
    print("----------\n"
          "from page:\n"
          "----------")
    print(df['company'].value_counts())


# Question 6: Find each company’s Higesht price car
def ex6(df):
    company_groupby = df.groupby('company')
    priceDf = company_groupby['company', 'price'].max()
    print(priceDf)


# Question 7: Find the average mileage of each car making company
def ex7(df):
    print("----------\n"
          "my solution:\n"
          "----------")
    company_groupby = df.groupby('company')
    avr_mileage = company_groupby['company', 'average-mileage'].mean()
    print(avr_mileage)


# Question 8: Sort all cars by Price column
def ex8(df):
    print("----------\n"
          "my solution:\n"
          "----------")
    sorted_df = df.sort_values(by='price', ascending=False)
    print(sorted_df.head(5))


# Question 9: Concatenate two data frames using the following conditions
# Create two data frames using the following two Dicts, Concatenate those two data frames and create a key for each data frame.
def ex9(df):
    GermanCars = {'Company': ['Ford', 'Mercedes', 'BMV', 'Audi'], 'Price': [23845, 171995, 135925, 71400]}
    japaneseCars = {'Company': ['Toyota', 'Honda', 'Nissan', 'Mitsubishi '], 'Price': [29995, 23600, 61500, 58900]}
    print("--- german_df ---")
    german_df = pd.DataFrame(GermanCars)
    print(german_df)
    print("--- japan_df ---")
    japan_df = pd.DataFrame(japaneseCars)
    print(japan_df)
    print("--- concat ---")
    concat_df = pd.concat([german_df, japan_df], keys=["Germany", "Japan"])
    print(concat_df)


def ex10(df):
    Car_Price = {'Company': ['Toyota', 'Honda', 'BMV', 'Audi'], 'Price': [23845, 17995, 135925, 71400]}
    carPriceDf = pd.DataFrame.from_dict(Car_Price)

    car_Horsepower = {'Company': ['Toyota', 'Honda', 'BMV', 'Audi'], 'horsepower': [141, 80, 182, 160]}
    carsHorsepowerDf = pd.DataFrame.from_dict(car_Horsepower)

    carsDf = pd.merge(carPriceDf, carsHorsepowerDf, on="Company")
    print(carsDf)


ex10(pd.read_csv(CSV_PATH))
