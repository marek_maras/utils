#!/bin/bash
# requires bash version >4
echo "bash version: ${BASH_VERSION}"
if [ -z "${BASH_VERSINFO}" ] || [ -z "${BASH_VERSINFO[0]}" ] || [ ${BASH_VERSINFO[0]} -lt 4 ] ]
then
    echo "Associative arrays used in this script require Bash version >= 4"; exit 1
elif [ ${BASH_VERSINFO[0]} -eq 4 ] && [ ${BASH_VERSINFO[1]} -lt 3 ]
then
    echo "local -n used in this script requires Bash version >= 4.3"; exit 1
fi


declare -A global_map=(
 ["k1"]="v1"
 ["k2"]="v2"
 ["k3"]="v3"
 )

f() {
  local -n local_map=$1
  for key in "${!local_map[@]}"
  do
    echo "key: ${key}"
    echo "value: ${local_map[$key]}"
  done
}

f global_map

