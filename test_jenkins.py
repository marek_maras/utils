import pytest

from jenkins import create_http_post_request


@pytest.mark.parametrize('params, expected_data', [
    ({'param': 'value'}, "param=value"),
    ({'p1': 'v1', 'p2': 'v2', 'p3': 'v3'}, "p1=v1&p2=v2&p3=v3"),
])
def test_create_http_post_request(params, expected_data):
    job_url = "http://jenkins.domain/job/job_name"
    token = "TOKEN"

    assert r'curl -X POST -v -d "{}" ' \
           r'http://jenkins.domain/job/job_name/buildWithParameters?token=TOKEN'.format(expected_data)\
           == create_http_post_request(job_url, params, token)
